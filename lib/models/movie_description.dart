import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bloc_implement/models/movie.dart';
import 'package:bloc_implement/widget/constants.dart';

class MovieDescriptionScreen extends StatelessWidget {
  const MovieDescriptionScreen({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent,
        onPressed: () {
          Navigator.pop(context);
        },
        child: Icon(Icons.cancel),
      ),

      body: CustomScrollView(
        slivers: [
          _buildSliverAppBar(),
          _buildMovieDetails(),
        ],
      ),
    );
  }

  Widget _buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 500,
      automaticallyImplyLeading: false,
      backgroundColor: Color.fromARGB(128, 255, 255, 255),

      pinned: true,
      floating: false,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '${movie.title}',
              style: GoogleFonts.belleza(
                fontSize: 17,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        background: ClipRRect(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(24),
            bottomRight: Radius.circular(24),
          ),
          child: Image.network(
            '${Constants.imagePath}${movie.backDropPath}',
            fit: BoxFit.cover,
            filterQuality: FilterQuality.high,
          ),
        ),
      ),
    );
  }

  Widget _buildMovieDetails() {
    return SliverToBoxAdapter(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          children: [
            Text(
              '${movie.overview}',
              style: GoogleFonts.abel(
                fontSize: 20,
                fontWeight: FontWeight.w800,
              ),
            ),
            SizedBox(height: 16),
            _buildAdditionalDetails(),
          ],
        ),
      ),
    );
  }

  Widget _buildAdditionalDetails() {
    return SizedBox(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildReleaseDateDetails(),
            _buildRatingDetails(),
          ],
        ),
      ),
    );
  }

  Widget _buildReleaseDateDetails() {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Text(
            'Release Date: ',
            style: GoogleFonts.roboto(
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Icon(Icons.date_range_outlined, color: Colors.amber),
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: Text(
              movie.releaseDate,
              style: GoogleFonts.roboto(
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRatingDetails() {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Text(
            'Rating: ',
            style: GoogleFonts.roboto(
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Icon(Icons.star, color: Colors.amber),
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: Text(
              '${movie.voteAverage.toStringAsFixed(1)}/10',
              style: GoogleFonts.roboto(
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
