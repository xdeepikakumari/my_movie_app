import 'package:flutter/material.dart';

class BackBtn extends StatefulWidget {
  const BackBtn({Key? key}) : super(key: key);

  @override
  _BackBtnState createState() => _BackBtnState();
}

class _BackBtnState extends State<BackBtn> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) {
        setState(() {
          isHovered = true;
        });
      },
      onExit: (event) {
        setState(() {
          isHovered = false;
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        height: 70,
        width: 70,
        margin: const EdgeInsetsDirectional.only(
          top: 8,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: isHovered
                ? [Colors.white, Colors.white]
                : [
                    Color.fromARGB(255, 35, 46, 37),
                    Color.fromARGB(255, 45, 56, 47),
                  ],
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_rounded),
          color: isHovered ? Colors.black : Colors.white,
        ),
      ),
    );
  }
}
