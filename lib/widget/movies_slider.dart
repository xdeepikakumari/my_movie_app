import 'package:bloc_implement/models/movie_description.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bloc_implement/models/movie.dart';
import 'package:bloc_implement/widget/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc_implement/bloc/movie_bloc.dart';
import 'package:bloc_implement/bloc/movie_state.dart';

class MoviesSlider extends StatelessWidget {
  const MoviesSlider({Key? key, required this.movies}) : super(key: key);
  final List<Movie> movies;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
        if (state is LoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        else if (state is ErrorState) {
          return Center(
            child: Text(
              'Error: ${state.errorMessage}',
              style: TextStyle(
                color: Color.fromARGB(255, 255, 246, 246),
                fontSize: 16,
              ),
            ),
          );
        }   else {
          return SizedBox(
            height: 200,
            width: double.infinity,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              itemCount: movies.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MovieDescriptionScreen(
                            movie: movies[index],
                          ),
                        ),
                      );
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: CachedNetworkImage(
                        imageUrl:
                            '${Constants.imagePath}${movies[index].posterPath}',
                        height: 180,
                        width: 150,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        }
      },
    );
  }
}
