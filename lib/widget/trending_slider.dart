import 'package:bloc_implement/models/movie_description.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc_implement/bloc/movie_bloc.dart';
import 'package:bloc_implement/bloc/movie_event.dart';
import 'package:bloc_implement/bloc/movie_state.dart';
import 'package:bloc_implement/models/movie.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:bloc_implement/widget/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:google_fonts/google_fonts.dart';

class TrendingSlider extends StatelessWidget {
  final List<Movie>? movieList;
  final movieListLength;

  const TrendingSlider(this.movieList,this.movieListLength);

  @override
  Widget build(BuildContext context) {
    final MovieBloc movieBloc = BlocProvider.of<MovieBloc>(context);
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
          return SizedBox(
            width: double.infinity,
            child: CarouselSlider.builder(
              itemCount: movieListLength,
              options: CarouselOptions(
                height: 300,
                autoPlay: true,
                viewportFraction: 0.4,
                enlargeCenterPage: true,
                pageSnapping: true,
                autoPlayCurve: Curves.fastOutSlowIn,
                autoPlayAnimationDuration: const Duration(seconds: 1),
              ),
              itemBuilder: (context, itemIndex, pageViewindex) {
                final Movie movie = movieList![itemIndex];
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MovieDescriptionScreen(
                          movie: movie,
                        ),
                      ),
                    );
                  },
                  child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: CachedNetworkImage(
                          imageUrl: '${Constants.imagePath}${movie.posterPath}',
                          height: 180,
                          width: 200,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Text(
                        '${movie.title}',
                        style: TextStyle(),
                      )
                    ],
                  ),
                );
              },
            ),
          );

      },
    );
  }
}
