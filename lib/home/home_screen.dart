
import 'package:bloc_implement/bloc/movie_bloc.dart';
import 'package:bloc_implement/bloc/movie_event.dart';
import 'package:bloc_implement/bloc/movie_state.dart';
import 'package:bloc_implement/home/search_barr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc_implement/widget/movies_slider.dart';
import 'package:bloc_implement/widget/trending_slider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movify'),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SearchBarWidget(),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Trending Movies',
                style: TextStyle(
                  fontSize: 25,
                  color: Color.fromARGB(255, 240, 16, 53),
                ),
              ),
            ),
            SizedBox(
              height: 200,
              child: BlocProvider(
                create: (context) => MovieBloc(),
                child: BlocBuilder<MovieBloc, MovieState>(
                  builder: (context, state) {
                    if (state is LoadingState) {
                      BlocProvider.of<MovieBloc>(context)
                          .add(FetchTrendingMoviesEvent());
                      return const Center(child: CircularProgressIndicator());
                    } else if (state is TrendingMoviesLoadedState) {
                      return TrendingSlider(
                          state.trendingMovies, state.trendingMovies.length);
                    } else if (state is ErrorState) {
                      return Center(
                        child: Text(
                          'Error: ${state.errorMessage}',
                          style: const TextStyle(
                            color: Color.fromARGB(255, 255, 246, 246),
                            fontSize: 16,
                          ),
                        ),
                      );
                    } else {
                      return const Center(child: Text('Unknown Error'));
                    }
                  },
                ),
              ),
            ),
            const SizedBox(height: 16),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Top Rated Movies',
                style: TextStyle(
                  fontSize: 25,
                  color: Color.fromARGB(255, 240, 16, 53),
                ),
              ),
            ),
            SizedBox(
              height: 200,
              child: BlocProvider(
                create: (context) => MovieBloc(),
                child: BlocBuilder<MovieBloc, MovieState>(
                  builder: (context, state) {
                    if (state is LoadingState) {
                      BlocProvider.of<MovieBloc>(context)
                          .add(FetchTopRatedMoviesEvent());
                      return const Center(child: CircularProgressIndicator());
                    } else if (state is TopRatedMoviesLoadedState) {
                      return MoviesSlider(movies: state.topRatedMovies);
                    } else if (state is ErrorState) {
                      return Center(
                        child: Text(
                          'Error: ${state.errorMessage}',
                          style: const TextStyle(
                            color: Color.fromARGB(255, 255, 246, 246),
                            fontSize: 16,
                          ),
                        ),
                      );
                    } else {
                      return const Center(child: Text('Unknown Error'));
                    }
                  },
                ),
              ),
            ),
            const SizedBox(height: 16),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Upcoming Movies',
                style: TextStyle(
                  fontSize: 25,
                  color: Color.fromARGB(255, 240, 16, 53),
                ),
              ),
            ),
            SizedBox(
              height: 200,
              child: BlocProvider(
                create: (context) => MovieBloc(),
                child: BlocBuilder<MovieBloc, MovieState>(
                  builder: (context, state) {
                    if (state is LoadingState) {
                      BlocProvider.of<MovieBloc>(context)
                          .add(FetchUpcomingMoviesEvent());
                      return const Center(child: CircularProgressIndicator());
                    } else if (state is UpcomingMoviesLoadedState) {
                      return MoviesSlider(movies: state.upcomingMovies);
                    } else if (state is ErrorState) {
                      return Center(
                        child: Text(
                          'Error: ${state.errorMessage}',
                          style: const TextStyle(
                            color: Color.fromARGB(255, 255, 246, 246),
                            fontSize: 16,
                          ),
                        ),
                      );
                    } else {
                      return const Center(child: Text('Unknown Error'));
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
