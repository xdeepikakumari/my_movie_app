import 'dart:convert';

import 'package:bloc_implement/bloc/movie_bloc.dart';
import 'package:bloc_implement/models/movie.dart';
import 'package:bloc_implement/models/movie_description.dart';
import 'package:bloc_implement/widget/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key, required this.movie});
  final List<Movie> movie;
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late Future<List<Movie>> _futureMovies;
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchAllMovies();
  }

  List<Movie> allMovies = [], srch = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Container(
            child: TextField(
              controller: _searchController,
              decoration: const InputDecoration(
                hintText: 'Search Movies',
                prefixIcon: Icon(Icons.search),
              ),
              onChanged: (value) {
                if (value.length == 0) {
                  srch = allMovies;
                  setState(() {});
                  return;
                }
                srch = [];

                srch = allMovies
                    .where((element) => element.title
                        .toLowerCase()
                        .contains(value.toLowerCase()))
                    .toList();
                List<Movie> newlist = srch;
                srch = [];
                for (Movie i in newlist) {
                  bool b = true;
                  for (Movie j in srch) {
                    if (i.title.contains(j.title)) b = false;
                  }
                  if (b) {
                    srch.add(i);
                  }
                }
                setState(() {});
              },
            ),
          ),
        ),
        body: Container(
          child: Column(
            children: [
              srch.length == 0
                  ? Column(
                      children: [
                        Center(
                          child: Text('Nothing there'),
                        ),
                      ],
                    )
                  : Expanded(
                      child: ListView.builder(
                        itemCount: srch!.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MovieDescriptionScreen(
                                    movie: srch[index],
                                  ),
                                ),
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              // child: Container(width: 100,height: 100,color: Colors.amber,child: Text(snapshot.data![index].),),
                              child: SizedBox(
                                height: 20, // Assuming posterHeight is the height of the poster image
                                child: Image.network(
                                  '${Constants.imagePath}${srch[index].posterPath}',
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    )
            ],
          ),
        ));
  }

  static const _trendingMoviesUrl =
      'https://api.themoviedb.org/3/trending/movie/day?api_key=${Constants.apiKey}';
  static const _topRatedMoviesUrl =
      'https://api.themoviedb.org/3/movie/top_rated?api_key=${Constants.apiKey}';
  static const _upcomingMoviesUrl =
      'https://api.themoviedb.org/3/movie/upcoming?api_key=${Constants.apiKey}';

  Future<List<Movie>> fetchAllMovies() async {
    List<Movie> trendingMovies = await _fetchMovies(_trendingMoviesUrl);
    allMovies.addAll(trendingMovies);

    List<Movie> topRatedMovies = await _fetchMovies(_topRatedMoviesUrl);
    allMovies.addAll(topRatedMovies);

    List<Movie> upcomingMovies = await _fetchMovies(_upcomingMoviesUrl);
    allMovies.addAll(upcomingMovies);

    srch = allMovies;
    setState(() {});
    return allMovies;
  }

  Future<List<Movie>> _fetchMovies(String url) async {
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final List<dynamic> responseData = jsonDecode(response.body)['results'];
      return responseData.map((json) => Movie.fromJson(json)).toList();
    } else {
      throw Exception('Failed to fetch movies');
    }
  }
}
