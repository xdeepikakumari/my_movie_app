import 'package:bloc_implement/home/search_screen.dart';
import 'package:flutter/material.dart';

class SearchBarWidget extends StatelessWidget {
  const SearchBarWidget({super.key});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return SearchScreen(
            movie: [],
          );
        }));
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey),
            color: Colors.black),
        height: 67,
        width: double.infinity,
        child: const Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'Search Movies',
            style: TextStyle(fontSize: 25),
          ),
        ),
      ),
    );
  }
}
