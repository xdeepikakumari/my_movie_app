

abstract class MovieEvent {}

class FetchTrendingMoviesEvent extends MovieEvent {}

class FetchTopRatedMoviesEvent extends MovieEvent {}

class FetchUpcomingMoviesEvent extends MovieEvent {}

class FetchAllMoviesEvent extends MovieEvent{
  
}
