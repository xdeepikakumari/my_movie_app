import 'dart:async';
import 'dart:convert';
import 'package:bloc_implement/api_call/api.dart';
import 'package:bloc_implement/bloc/movie_event.dart';
import 'package:bloc_implement/bloc/movie_state.dart';
import 'package:bloc_implement/models/movie.dart';
import 'package:bloc_implement/widget/constants.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive/hive.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc() : super(LoadingState()) {
    on<FetchTrendingMoviesEvent>(fetchTrendingMovies);
    on<FetchTopRatedMoviesEvent>(fetchTopRatedMovies);
    on<FetchUpcomingMoviesEvent>(fetchUpcomingMovies);
    on<FetchAllMoviesEvent>(fetchAllMovies);
  }

  List? trendingmovies = [];
  List<Map<String, dynamic>> trendingCachedMovies = [];
  Future<void> fetchTrendingMovies(
      FetchTrendingMoviesEvent event, Emitter<MovieState> emit) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      final appDocumentDirectory =
          await path_provider.getApplicationDocumentsDirectory();
      Hive.init(appDocumentDirectory.path);
      await Hive.openBox('apiCache');
      List<dynamic> c = Hive.box('apiCache').get('response', defaultValue: []);
      List<Movie> emptylist = [];
      for (var i in c) {
        emptylist.add(Movie.fromJson(i));
      }
      emit(TrendingMoviesLoadedState(emptylist));
    } else {
      try {
        emit(LoadingState());
        http.Response response;
        response = await http.get(Uri.parse(
            'https://api.themoviedb.org/3/trending/movie/day?api_key=${Constants.apiKey}'));
        if (response.statusCode == 200) {
          trendingmovies = json.decode(response.body)?['results'];
          List<Movie> movies =
              trendingmovies!.map((movie) => Movie.fromJson(movie)).toList();
          List<Map<String, dynamic>> movieMaps =
              movies.map((movie) => movie.toJson()).toList();
          final appDocumentDirectory =
              await path_provider.getApplicationDocumentsDirectory();
          Hive.init(appDocumentDirectory.path);
          await Hive.openBox('apiCache');
          await Hive.box('apiCache').put('response', movieMaps);
          List<Map<String, dynamic>> c =
              Hive.box('apiCache').get('response', defaultValue: []);
          emit(TrendingMoviesLoadedState(movies));
        }
      } catch (e) {
        trendingCachedMovies =
            Hive.box('apiCache').get('response', defaultValue: []);
        List<Map<String, dynamic>> c =
            Hive.box('apiCache').get('response', defaultValue: []);
        emit(TrendingMoviesLoadedState(
            c.map((e) => Movie.fromJson(e)).toList()));
      }
    }
  }

  List? topratedmovies = [];
  List<Map<String, dynamic>> topratedCachedMovies = [];
  Future<void> fetchTopRatedMovies(
      FetchTopRatedMoviesEvent event, Emitter<MovieState> emit) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      final appDocumentDirectory =
          await path_provider.getApplicationDocumentsDirectory();
      Hive.init(appDocumentDirectory.path);
      await Hive.openBox('apiCache');
      List<dynamic> cachedData =
          Hive.box('apiCache').get('topRatedMovies', defaultValue: []);
      List<Movie> cachedMovies = [];
      for (var data in cachedData) {
        cachedMovies.add(Movie.fromJson(data));
      }
      emit(TopRatedMoviesLoadedState(cachedMovies));
    } else {
      try {
        emit(LoadingState());
        http.Response response;
        response = await http.get(Uri.parse(
            'https://api.themoviedb.org/3/movie/top_rated?api_key=${Constants.apiKey}'));
        if (response.statusCode == 200) {
          List<Map<String, dynamic>> movieData =
              List<Map<String, dynamic>>.from(
                  json.decode(response.body)['results']);
          List<Movie> movies =
              movieData.map((movie) => Movie.fromJson(movie)).toList();
          final appDocumentDirectory =
              await path_provider.getApplicationDocumentsDirectory();
          Hive.init(appDocumentDirectory.path);
          await Hive.openBox('apiCache');
          await Hive.box('apiCache').put('topRatedMovies', movieData);
          emit(TopRatedMoviesLoadedState(movies));
        }
      } catch (e) {
        List<Map<String, dynamic>> cachedData =
            Hive.box('apiCache').get('topRatedMovies', defaultValue: []);
        List<Movie> cachedMovies = [];
        for (var data in cachedData) {
          cachedMovies.add(Movie.fromJson(data));
        }
        emit(TopRatedMoviesLoadedState(cachedMovies));
      }
    }
  }

  List? upcomingmovies = [];
  List<Map<String, dynamic>> upcomingCachedMovies = [];
  Future<void> fetchUpcomingMovies(
      FetchUpcomingMoviesEvent event, Emitter<MovieState> emit) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      final appDocumentDirectory =
          await path_provider.getApplicationDocumentsDirectory();
      Hive.init(appDocumentDirectory.path);
      await Hive.openBox('upcomingApiCache');
      List<dynamic> cachedData =
          Hive.box('upcomingApiCache').get('response', defaultValue: []);
      List<Movie> emptyList = [];
      for (var data in cachedData) {
        emptyList.add(Movie.fromJson(data));
      }
      emit(UpcomingMoviesLoadedState(emptyList));
    } else {
      try {
        emit(LoadingState());
        http.Response response;
        response = await http.get(Uri.parse(
            'https://api.themoviedb.org/3/movie/upcoming?api_key=${Constants.apiKey}'));
        if (response.statusCode == 200) {
          upcomingmovies = json.decode(response.body)?['results'];
          List<Movie> movies =
              upcomingmovies!.map((movie) => Movie.fromJson(movie)).toList();
          List<Map<String, dynamic>> movieMaps =
              movies.map((movie) => movie.toJson()).toList();
          final appDocumentDirectory =
              await path_provider.getApplicationDocumentsDirectory();
          Hive.init(appDocumentDirectory.path);
          await Hive.openBox('upcomingApiCache');
          await Hive.box('upcomingApiCache').put('response', movieMaps);
          emit(UpcomingMoviesLoadedState(movies));
        }
      } catch (e) {
        upcomingCachedMovies =
            Hive.box('upcomingApiCache').get('response', defaultValue: []);
        emit(UpcomingMoviesLoadedState(
            upcomingCachedMovies.map((e) => Movie.fromJson(e)).toList()));
      }
    }
  }

  Future<void> fetchAllMovies(
      FetchAllMoviesEvent event, Emitter<MovieState> emit) async {
    emit(LoadingState());
    final List<Movie> trendingMovies = await Api().getTrendingMovies();
    final List<Movie> topRatedMovies = await Api().gettopRatedMovies();
    final List<Movie> upcomingMovies = await Api().getUpcomingMovies();
    final List<Movie> allMovies = [];
    if (trendingMovies.isEmpty == false &&
        topRatedMovies.isEmpty == false &&
        upcomingMovies.isEmpty == false) {
      allMovies
        ..addAll(trendingMovies)
        ..addAll(topRatedMovies)
        ..addAll(upcomingMovies);
    }
    try {
      emit(AllMoviesLoadedState(allMovies: allMovies));
    } catch (e) {
      emit(ErrorState('Failed to fetch all Movies : $e'));
    }
  }
}
