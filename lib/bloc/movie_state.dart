import 'package:bloc_implement/models/movie.dart';

abstract class MovieState{}

class LoadingState extends MovieState{}

class TrendingMoviesLoadedState extends MovieState{
  final List<Movie> trendingMovies;
  TrendingMoviesLoadedState(this.trendingMovies,);
}

class TopRatedMoviesLoadedState extends MovieState{
  final List<Movie> topRatedMovies;
  TopRatedMoviesLoadedState(this.topRatedMovies);
}

class UpcomingMoviesLoadedState extends MovieState{
  final List<Movie> upcomingMovies;
  UpcomingMoviesLoadedState(this.upcomingMovies);
}

class AllMoviesLoadedState extends MovieState {
  final List<Movie> allMovies;
  AllMoviesLoadedState({required this.allMovies}) : super();
  @override
  List<Movie> get props => allMovies;
}

class ErrorState extends MovieState{
  final String errorMessage;
  ErrorState(this.errorMessage);
}